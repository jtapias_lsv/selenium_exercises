
from selenium import webdriver
import getpass
import time

"""
    This function call the driver path of google to manage the browser
    open the url of facebook, wait for 3 seconds, linked the fields 
    email and password in facebook's login page and the buttom login,
    then send text of the parameters to login this account and for last
    click and login buttom 
"""
def open_facebook(email, password, post):
    browser= webdriver.Chrome('/home/jumar98/driver/chromedriver')
    browser.get('https://facebook.com')
    time.sleep(2)
    email_field = browser.find_element_by_name('email')
    password_field = browser.find_element_by_name('pass')
    login_button = browser.find_element_by_id('u_0_2')
    email_field.send_keys(email)
    time.sleep(2)
    password_field.send_keys(password)
    time.sleep(2)
    login_button.click()

    # Here we post in facebook

    # Select the xpath to the element to post
    statusbox = browser.find_element_by_xpath("//*[@name='xhpc_message']")
    time.sleep(2)
    # Send the post
    statusbox.send_keys(post)
    # Execute script to click, button post the facebook, becouse is hidden or desactivate for default
    browser.execute_script("document.querySelectorAll('.selected')[1].click()")
    time.sleep(5)
    # Close the browser
    browser.close()

# This is the main, here we call open_facebook and we catch email and password
if __name__ == '__main__':
    email = input("Ingrese su email: ")
    password = getpass.getpass("Ingrese su contraseña: ")
    post = input("Escribe tu post: ")
    open_facebook(email,password,post)